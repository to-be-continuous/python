## [7.9.1](https://gitlab.com/to-be-continuous/python/compare/7.9.0...7.9.1) (2025-03-11)


### Bug Fixes

* **bump-my-version:** improve bump-my-version config verification (solves [#106](https://gitlab.com/to-be-continuous/python/issues/106)) ([64b624a](https://gitlab.com/to-be-continuous/python/commit/64b624a4d0abde429d50a00a9c595993c369fbd0))

# [7.9.0](https://gitlab.com/to-be-continuous/python/compare/7.8.3...7.9.0) (2025-03-10)


### Features

* skip GCP ADC authent when GCP_JWT is not present ([b43207f](https://gitlab.com/to-be-continuous/python/commit/b43207f6eee26a8d17bc75ed19b54208534b3ad9))

## [7.8.3](https://gitlab.com/to-be-continuous/python/compare/7.8.2...7.8.3) (2025-02-23)


### Bug Fixes

* change _pip to pass cmd then PIP_OPTS ([c1b277e](https://gitlab.com/to-be-continuous/python/commit/c1b277e31b977b41eedd5e213e7672d11c66da33))

## [7.8.2](https://gitlab.com/to-be-continuous/python/compare/7.8.1...7.8.2) (2025-02-03)


### Bug Fixes

* **gcp:** reduce scope of GCP App Default Creds script to template ([829bfce](https://gitlab.com/to-be-continuous/python/commit/829bfceffe3a2e097914c719d4a4488d544be7ab))

## [7.8.1](https://gitlab.com/to-be-continuous/python/compare/7.8.0...7.8.1) (2025-01-31)


### Bug Fixes

* **sbom:** only generate SBOMs on prod branches, integ branches and release tags ([8da756f](https://gitlab.com/to-be-continuous/python/commit/8da756f273cb22dbd12c866ba1e6f7f07b52cb4a))

# [7.8.0](https://gitlab.com/to-be-continuous/python/compare/7.7.1...7.8.0) (2025-01-27)


### Features

* disable tracking service by default ([7237a96](https://gitlab.com/to-be-continuous/python/commit/7237a96b5014006853865c3c2c29b2eb008f144c))

## [7.7.1](https://gitlab.com/to-be-continuous/python/compare/7.7.0...7.7.1) (2025-01-12)


### Bug Fixes

* move back 'reports' dir creation at job level to fix variants missing reports dir ([bf15efe](https://gitlab.com/to-be-continuous/python/commit/bf15efe4b008a5f292e782d0363a52000bf43f37))

# [7.7.0](https://gitlab.com/to-be-continuous/python/compare/7.6.0...7.7.0) (2025-01-12)


### Features

* add auto-release as an optional feature for releases ([9db709a](https://gitlab.com/to-be-continuous/python/commit/9db709ad8fe96c7ed524f8083e57b845914e4009))

# [7.6.0](https://gitlab.com/to-be-continuous/python/compare/7.5.2...7.6.0) (2025-01-08)


### Features

* add separate 'publish-enabled' to enable publishing package ([6f9ee56](https://gitlab.com/to-be-continuous/python/commit/6f9ee56d00ee5408953fa24323dbba81aa2d4f3a))

## [7.5.2](https://gitlab.com/to-be-continuous/python/compare/7.5.1...7.5.2) (2024-12-22)


### Bug Fixes

* **test:** handle decimal coverage ([4fb81f8](https://gitlab.com/to-be-continuous/python/commit/4fb81f8b66bf285f173a2335f8c34523d0f7ca3d))

## [7.5.1](https://gitlab.com/to-be-continuous/python/compare/7.5.0...7.5.1) (2024-11-21)


### Bug Fixes

* **CodeArtifact:** fix AWS CodeArtifact variant ([c913e65](https://gitlab.com/to-be-continuous/python/commit/c913e6538d88efaf1d6f0eb7742e7531d66a32c2))

# [7.5.0](https://gitlab.com/to-be-continuous/python/compare/7.4.0...7.5.0) (2024-11-11)


### Features

* **Ruff:** add `ruff-format` job for code formatting ([142589f](https://gitlab.com/to-be-continuous/python/commit/142589f2c260336d3a703af3e149c1c666fd5373))

# [7.4.0](https://gitlab.com/to-be-continuous/python/compare/7.3.3...7.4.0) (2024-11-08)


### Features

* add AWS CodeArtifact support (variant) ([128fb99](https://gitlab.com/to-be-continuous/python/commit/128fb9950c1354c211abe17d5cba19d75dd66ecc))

## [7.3.3](https://gitlab.com/to-be-continuous/python/compare/7.3.2...7.3.3) (2024-11-06)


### Bug Fixes

* correct bandit exclude of .venv and .cache ([ed95527](https://gitlab.com/to-be-continuous/python/commit/ed955279f56f2d66a2a7532b35515f2309f05f5c)), closes [#92](https://gitlab.com/to-be-continuous/python/issues/92)

## [7.3.2](https://gitlab.com/to-be-continuous/python/compare/7.3.1...7.3.2) (2024-11-02)


### Bug Fixes

* limit security reports access to developer role or higher ([40c85ef](https://gitlab.com/to-be-continuous/python/commit/40c85eff562a00ceb9b381ef72472ce1910b97ab))

## [7.3.1](https://gitlab.com/to-be-continuous/python/compare/7.3.0...7.3.1) (2024-10-25)


### Bug Fixes

* **Trivy:** trivy scan fails when issues are found ([671b781](https://gitlab.com/to-be-continuous/python/commit/671b78142c08cdd5bbf1441a81705b96dbf0740f))
* use right options for uv with extras deps ([354af5a](https://gitlab.com/to-be-continuous/python/commit/354af5ad8294ad8f3de3f7ad6aeaf8752d5f2625))

# [7.3.0](https://gitlab.com/to-be-continuous/python/compare/7.2.0...7.3.0) (2024-10-15)


### Features

* **uv:** add uv support as a new build system ([8aeb20b](https://gitlab.com/to-be-continuous/python/commit/8aeb20b09347ff35398a4a707852a9cc17cc6842)), closes [#80](https://gitlab.com/to-be-continuous/python/issues/80)
* **uv:** add uv support as a new build system ([d22ffba](https://gitlab.com/to-be-continuous/python/commit/d22ffbacb4228cb4ffdc6396bca9e43ad194bfff))

# [7.2.0](https://gitlab.com/to-be-continuous/python/compare/7.1.1...7.2.0) (2024-10-04)


### Bug Fixes

* **release:** support full semantic-versioning specifcation (with prerelease and build metadata) ([08e9d7e](https://gitlab.com/to-be-continuous/python/commit/08e9d7e9f7f1bdd43a2070c9ee5abb16a8b8aaa0))
* **trivy:** use --pkg-types instead of deprecated --vuln-type option ([5e0a0d2](https://gitlab.com/to-be-continuous/python/commit/5e0a0d2918fd7539bd2e1cb955e99ef5857db1f5))


### Features

* **trivy:** enable comprehensive priority ([322eb1b](https://gitlab.com/to-be-continuous/python/commit/322eb1b88c49d9a1662ad6b6199541f1a82860ef))

## [7.1.1](https://gitlab.com/to-be-continuous/python/compare/7.1.0...7.1.1) (2024-10-03)


### Bug Fixes

* Poetry Build system test ([9505604](https://gitlab.com/to-be-continuous/python/commit/95056049e7ee8239b6358def7c594e7002036574))

# [7.1.0](https://gitlab.com/to-be-continuous/python/compare/7.0.2...7.1.0) (2024-09-15)


### Bug Fixes

* check trivy activity to match new log format ([edd8fcf](https://gitlab.com/to-be-continuous/python/commit/edd8fcf71f1b251c467d6bbce6e8a190d4584dda))
* pylint --ignore .cache not working now use find to exclude .cache ([e1463bc](https://gitlab.com/to-be-continuous/python/commit/e1463bc750fbd24b12d407267061d8ae8a3718f1))


### Features

* isort exclude .cache ([e333183](https://gitlab.com/to-be-continuous/python/commit/e333183ca48aa98baf9d510caf0c8f3f93d04b82))
* remove unnecesary install when use poetry or pipenv ([f025c6d](https://gitlab.com/to-be-continuous/python/commit/f025c6df22d48bd735458fc478b18d2235a715a2))

## [7.0.2](https://gitlab.com/to-be-continuous/python/compare/7.0.1...7.0.2) (2024-05-20)


### Bug Fixes

* issue [#73](https://gitlab.com/to-be-continuous/python/issues/73) github_get_latest_version ([ce26d5a](https://gitlab.com/to-be-continuous/python/commit/ce26d5abba8950f30bad1d992a2481bf252359b7))
* README for trivy now enabled by default ([f5d5f2e](https://gitlab.com/to-be-continuous/python/commit/f5d5f2e9c186b6aeb0c55ef45a65b85615b9ad7b))

## [7.0.1](https://gitlab.com/to-be-continuous/python/compare/7.0.0...7.0.1) (2024-05-17)


### Bug Fixes

* remove useless apt commands ([469ebec](https://gitlab.com/to-be-continuous/python/commit/469ebece1e05b4748c3bd4e2ff793b058ea57bbf))

# [7.0.0](https://gitlab.com/to-be-continuous/python/compare/6.11.1...7.0.0) (2024-05-17)


### Code Refactoring

* **Trivy:** py-trivy job is enabled by default ([d9e8679](https://gitlab.com/to-be-continuous/python/commit/d9e867952d5a2040037c5cb03456380b976219af))


### Features

* add support for slim and alpine Python images + change default base image ([22003d1](https://gitlab.com/to-be-continuous/python/commit/22003d195670f3d923f31014636485bc783068bb))


### BREAKING CHANGES

* **Trivy:** py-trivy job is enabled by default
'trivy-enabled' input is no longer supported - use 'trivy-disabled' instead
* the default base image has been changed to python:debian-slim

## [6.11.1](https://gitlab.com/to-be-continuous/python/compare/6.11.0...6.11.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([8703dea](https://gitlab.com/to-be-continuous/python/commit/8703dea915ca48038588adcc2a0672269dd24bb4))

# [6.11.0](https://gitlab.com/to-be-continuous/python/compare/6.10.0...6.11.0) (2024-04-28)


### Bug Fixes

* **cache:** always keep cache ([8cf171e](https://gitlab.com/to-be-continuous/python/commit/8cf171edde7577b4179530192169730eff122ff2))


### Features

* add mypy job ([cc22e8c](https://gitlab.com/to-be-continuous/python/commit/cc22e8c0eda56d2df00e22b8034da40fd2cfc937))

# [6.10.0](https://gitlab.com/to-be-continuous/python/compare/6.9.0...6.10.0) (2024-04-14)


### Features

* **ruff:** generate JSON report when SonarQube is detected ([81e711d](https://gitlab.com/to-be-continuous/python/commit/81e711d2cd6e23014c0eaef2ec098fe69b711885))

# [6.9.0](https://gitlab.com/to-be-continuous/python/compare/6.8.1...6.9.0) (2024-04-13)


### Features

* add ⚡ Ruff linter job ([ef364ef](https://gitlab.com/to-be-continuous/python/commit/ef364ef65dad8034b7c7af60bb25195cf20ba466))
* some log improvement ([aef5a05](https://gitlab.com/to-be-continuous/python/commit/aef5a056d09b22e2c6ce2fa8038632c912e11637))

## [6.8.1](https://gitlab.com/to-be-continuous/python/compare/6.8.0...6.8.1) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([4342c01](https://gitlab.com/to-be-continuous/python/commit/4342c01475a4e227c4c4e06af4be0c016b2da2a2))

# [6.8.0](https://gitlab.com/to-be-continuous/python/compare/6.7.0...6.8.0) (2024-04-02)


### Features

* add GCP auth variant ([0188ac9](https://gitlab.com/to-be-continuous/python/commit/0188ac9d4aa8e952060ebc3e485a7c298cc5d4b0))

# [6.7.0](https://gitlab.com/to-be-continuous/python/compare/6.6.5...6.7.0) (2024-2-21)


### Features

* add black job ([054b040](https://gitlab.com/to-be-continuous/python/commit/054b0403499164be5f046ff694e6f8ef94a487ca))
* add isort job ([e8e068b](https://gitlab.com/to-be-continuous/python/commit/e8e068ba46130a99ba0c99005d2690b8640a75aa))

## [6.6.5](https://gitlab.com/to-be-continuous/python/compare/6.6.4...6.6.5) (2024-2-15)


### Bug Fixes

* more reliable retrieve url for Syft ([5d83eaa](https://gitlab.com/to-be-continuous/python/commit/5d83eaad9bfc08b10c41656f457c37fbec1ebb5b)), closes [#65](https://gitlab.com/to-be-continuous/python/issues/65)

## [6.6.4](https://gitlab.com/to-be-continuous/python/compare/6.6.3...6.6.4) (2024-2-2)


### Bug Fixes

* sanitize variable substitution pattern ([c1cf8c3](https://gitlab.com/to-be-continuous/python/commit/c1cf8c387f37dfdc29958d9168f13736707f135f))

## [6.6.3](https://gitlab.com/to-be-continuous/python/compare/6.6.2...6.6.3) (2024-1-26)


### Bug Fixes

* resolve "python-index-cataloger does not exist" ([69531a8](https://gitlab.com/to-be-continuous/python/commit/69531a8e11fd941683177afcbc2b2ebe6f552085))

## [6.6.2](https://gitlab.com/to-be-continuous/python/compare/6.6.1...6.6.2) (2024-1-22)


### Bug Fixes

* avoid rate limiting for latest syft url query ([21ce764](https://gitlab.com/to-be-continuous/python/commit/21ce76493510502c1e654ca50cfc8913f2928712))

## [6.6.1](https://gitlab.com/to-be-continuous/python/compare/6.6.0...6.6.1) (2023-12-19)


### Bug Fixes

* add an info message when pyproject.toml found with no 'build-backend' ([b4d95fb](https://gitlab.com/to-be-continuous/python/commit/b4d95fb0a8d3dfe442abd94e7c419d6ef86e4d4b)), closes [#57](https://gitlab.com/to-be-continuous/python/issues/57)
* **bandit:** exclude .cache/ dir ([d62f2a2](https://gitlab.com/to-be-continuous/python/commit/d62f2a2dcf24af9feebe6483579589d59ff2c3b5)), closes [#58](https://gitlab.com/to-be-continuous/python/issues/58)

# [6.6.0](https://gitlab.com/to-be-continuous/python/compare/6.5.0...6.6.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([046d949](https://gitlab.com/to-be-continuous/python/commit/046d949314260b029236a85f0141c81b828eea60))

# [6.5.0](https://gitlab.com/to-be-continuous/python/compare/6.4.1...6.5.0) (2023-12-01)

 
### Features

* support CI/CD component design ([0166bd4](https://gitlab.com/to-be-continuous/python/commit/0166bd43891f4bab8934e4379e8c34f399d681f5))
* **version**: minimal GitLab version requirements: 16.6

## [6.4.1](https://gitlab.com/to-be-continuous/python/compare/6.4.0...6.4.1) (2023-11-25)


### Bug Fixes

* switch from bumpversion to bump-my-version ([8b5c299](https://gitlab.com/to-be-continuous/python/commit/8b5c29923f11fc7806f9822f7181bb5d560c8f75))

# [6.4.0](https://gitlab.com/to-be-continuous/python/compare/6.3.5...6.4.0) (2023-11-02)


### Features

* add vault variant ([bf678c4](https://gitlab.com/to-be-continuous/python/commit/bf678c4d300c77f3805b016f9a74a29b4c520af5))

## [6.3.5](https://gitlab.com/to-be-continuous/python/compare/6.3.4...6.3.5) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([d8697c9](https://gitlab.com/to-be-continuous/python/commit/d8697c98a0cf94e471035e518a62202682850508))

## [6.3.4](https://gitlab.com/to-be-continuous/python/compare/6.3.3...6.3.4) (2023-09-22)


### Bug Fixes

* **bumpversion:** fix bumpversion CLI arguments ([7d95ca1](https://gitlab.com/to-be-continuous/python/commit/7d95ca15781e83b8bba05394f095bd5ffc211fc8)), closes [#53](https://gitlab.com/to-be-continuous/python/issues/53)

## [6.3.3](https://gitlab.com/to-be-continuous/python/compare/6.3.2...6.3.3) (2023-08-09)


### Bug Fixes

* add component name to sbom ([6cb2719](https://gitlab.com/to-be-continuous/python/commit/6cb271973e7bdcd2a297b96c16a3a13dd42296be))

## [6.3.2](https://gitlab.com/to-be-continuous/python/compare/6.3.1...6.3.2) (2023-07-07)


### Bug Fixes

* **package:** expire built artifacts ([d4f7b4a](https://gitlab.com/to-be-continuous/python/commit/d4f7b4aa24c2ae220b98fa56d66aaafe5794fbc7))

## [6.3.1](https://gitlab.com/to-be-continuous/python/compare/6.3.0...6.3.1) (2023-06-27)


### Bug Fixes

* **publish:** avoid rebuilding packages (optimization) ([0002705](https://gitlab.com/to-be-continuous/python/commit/0002705aa22716db026a90f6a899ef786efcbe35))

# [6.3.0](https://gitlab.com/to-be-continuous/python/compare/6.2.0...6.3.0) (2023-05-28)


### Features

* **release:** implement 2 steps release ([1a58dd1](https://gitlab.com/to-be-continuous/python/commit/1a58dd1908c6a210ff9a285bc2a61b0fdcb1410b))
* **release:** make release commit message configurable ([2eba9cf](https://gitlab.com/to-be-continuous/python/commit/2eba9cf352099a0cefa930d59ff995e8dca06d56))

# [6.2.0](https://gitlab.com/to-be-continuous/python/compare/6.1.5...6.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([ba47f0e](https://gitlab.com/to-be-continuous/python/commit/ba47f0ee543aef7f80f33a10e023c5f214e3b7b1))

## [6.1.5](https://gitlab.com/to-be-continuous/python/compare/6.1.4...6.1.5) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([6525c1c](https://gitlab.com/to-be-continuous/python/commit/6525c1cf39fe2e88039122a523bc96e6b1903431))

## [6.1.4](https://gitlab.com/to-be-continuous/python/compare/6.1.3...6.1.4) (2023-03-18)


### Bug Fixes

* **bumpversion:** use bumpversion configuration in priority if cfg file found ([cd4abb3](https://gitlab.com/to-be-continuous/python/commit/cd4abb30aa8670de948c5a0e76f3ccff9144436e))

## [6.1.3](https://gitlab.com/to-be-continuous/python/compare/6.1.2...6.1.3) (2023-01-29)


### Bug Fixes

* replace wget and curl by python scripts ([ef78ec7](https://gitlab.com/to-be-continuous/python/commit/ef78ec7d44549cd2909065ef767de9740ed0de82))

## [6.1.2](https://gitlab.com/to-be-continuous/python/compare/6.1.1...6.1.2) (2023-01-28)


### Bug Fixes

* PYTHON_PROJECT_DIR support for py-sbom ([2e7dc62](https://gitlab.com/to-be-continuous/python/commit/2e7dc62ea4d91ac4af80ca3652547992db71c966))

## [6.1.1](https://gitlab.com/to-be-continuous/python/compare/6.1.0...6.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([5650f7e](https://gitlab.com/to-be-continuous/python/commit/5650f7e213814fa6b5e78353d089fd3aac24f7d5))

# [6.1.0](https://gitlab.com/to-be-continuous/python/compare/6.0.2...6.1.0) (2022-12-13)


### Features

* add a job generating software bill of materials ([4c56888](https://gitlab.com/to-be-continuous/python/commit/4c56888e589c11ac60edf79f2aa9dcd23bda9062))

## [6.0.2](https://gitlab.com/to-be-continuous/python/compare/6.0.1...6.0.2) (2022-10-17)


### Bug Fixes

* bumpversion when using only setup.py. Refs: [#36](https://gitlab.com/to-be-continuous/python/issues/36) ([2944687](https://gitlab.com/to-be-continuous/python/commit/294468773ed6f60eaf93ee345047a65bcd4c78b4))

## [6.0.1](https://gitlab.com/to-be-continuous/python/compare/6.0.0...6.0.1) (2022-10-04)


### Bug Fixes

* **bandit:** fix shell syntax error ([bb64f96](https://gitlab.com/to-be-continuous/python/commit/bb64f96f228f13da9204567cb45a1d62d53ca121))

# [6.0.0](https://gitlab.com/to-be-continuous/python/compare/5.1.0...6.0.0) (2022-10-04)


### Features

* normalize reports ([d591f6d](https://gitlab.com/to-be-continuous/python/commit/d591f6d1f2e5469dc8e926e2860f25516e64a820))


### BREAKING CHANGES

* generated reports have changed (see doc). It is a breaking change if you're using SonarQube.

# [5.1.0](https://gitlab.com/to-be-continuous/python/compare/5.0.0...5.1.0) (2022-09-11)


### Features

* add ability to setup build tool version in PYTHON_BUILD_SYSTEM ([5bea2dd](https://gitlab.com/to-be-continuous/python/commit/5bea2dd8e9e4e50cdf6f0c7c6ef882e82b97c7d6))

# [5.0.0](https://gitlab.com/to-be-continuous/python/compare/4.2.0...5.0.0) (2022-08-05)


### Features

* adaptive pipeline rules ([543b4fe](https://gitlab.com/to-be-continuous/python/commit/543b4fe6e80ff0921d08a64f412308c128708a4d))
* switch to Merge Request pipelines as default ([714e066](https://gitlab.com/to-be-continuous/python/commit/714e066c2e4e33a5e109446c410e50f86e32f899))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [4.2.0](https://gitlab.com/to-be-continuous/python/compare/4.1.1...4.2.0) (2022-06-10)


### Features

* **lint:** add a report for SonarQube ([ba73998](https://gitlab.com/to-be-continuous/python/commit/ba7399884688452762d5d2d873f6ae82ab026a50))

## [4.1.1](https://gitlab.com/to-be-continuous/python/compare/4.1.0...4.1.1) (2022-05-06)


### Bug Fixes

* Manage deprecation for artifacts:report:cobertura ([9d7bcf3](https://gitlab.com/to-be-continuous/python/commit/9d7bcf393212c73ed16dd306abcd5a82ff04e5e5))

# [4.1.0](https://gitlab.com/to-be-continuous/python/compare/4.0.1...4.1.0) (2022-05-01)


### Bug Fixes

* migrate deprecated CI_BUILD_REF_NAME variable ([eb115a2](https://gitlab.com/to-be-continuous/python/commit/eb115a2391cb582f50fdec9564c177b1d71c2fa7))


### Features

* configurable tracking image ([2a0229f](https://gitlab.com/to-be-continuous/python/commit/2a0229fe98e0513d6847ddc621d1f5f8cb0cc1b1))

## [4.0.1](https://gitlab.com/to-be-continuous/python/compare/4.0.0...4.0.1) (2022-03-22)


### Bug Fixes

* **Trivy:** Scan transitive dependencies ([11d96db](https://gitlab.com/to-be-continuous/python/commit/11d96db74d03e157927177dd87438e9575111086))

# [4.0.0](https://gitlab.com/to-be-continuous/python/compare/3.2.1...4.0.0) (2022-02-25)


### Bug Fixes

* **Poetry:** Poetry cache in GitLab CI cache ([9fbaa6d](https://gitlab.com/to-be-continuous/python/commit/9fbaa6db687746c0c223caf01bf745f7eac91abb))


### chore

* renamed unprefixed variables ([8c8a873](https://gitlab.com/to-be-continuous/python/commit/8c8a873b795c4f8a6a8f07e9ed7729d9c35dacd5))


### Features

* add multi build-system support (Poetry, Setuptools or requirements file) ([130e210](https://gitlab.com/to-be-continuous/python/commit/130e2102af56dc8719ba5c87a7e31902fb9fe228))
* add Pipenv support ([7afc0db](https://gitlab.com/to-be-continuous/python/commit/7afc0dbfccfe6b7678cce2d6a9f7f7ececff193f))
* **release:** complete release process refactoring ([ff8b985](https://gitlab.com/to-be-continuous/python/commit/ff8b9856a0bb045932f4810410404261cd848ea4))


### BREAKING CHANGES

* **release:** complete refactoring or release process, including variables and jobs redefinition
- no more separate publish job: the entire release process is now performed by the py-release job
- TWINE_XXX variables removed and replaced by PYTHON_REPOSITORY_XXX
- RELEASE_VERSION_PART variable replaced by PYTHON_RELEASE_NEXT

For additional info, see the doc.
* rename $REQUIREMENTS_FILE as $PYTHON_REQS_FILE and $PYTHON_REQS_FILE as $PYTHON_EXTRA_REQS_FILES
default extra requirements changed from 'test-requirements.txt' to 'requirements-dev.txt'
* removed $PYTHON_POETRY_DISABLED with $PYTHON_BUILD_SYSTEM (see doc)

## [3.2.1](https://gitlab.com/to-be-continuous/python/compare/3.2.0...3.2.1) (2021-12-21)


### Bug Fixes

* **safety:** fix command not found when poetry is used ([1ee673b](https://gitlab.com/to-be-continuous/python/commit/1ee673b8e323d4e5733ae3cf91eb74fdc5393a9e))

# [3.2.0](https://gitlab.com/to-be-continuous/python/compare/3.1.1...3.2.0) (2021-12-20)


### Features

* add Trivy dependency scanner ([f0faed0](https://gitlab.com/to-be-continuous/python/commit/f0faed09819de9b45d748757d75bbf7680a69bce))

## [3.1.1](https://gitlab.com/to-be-continuous/python/compare/3.1.0...3.1.1) (2021-12-17)


### Bug Fixes

* switch from safety image to install safety ([e2b42c4](https://gitlab.com/to-be-continuous/python/commit/e2b42c407cf70ab9967977bbbfe745f6547a6ca1))

# [3.1.0](https://gitlab.com/to-be-continuous/python/compare/3.0.1...3.1.0) (2021-12-10)


### Features

* **publish:** configure the GitLab Packages registry as default Python registry for publish ([891c32a](https://gitlab.com/to-be-continuous/python/commit/891c32aecc986599417ff2404f83eaff66ee4400))

## [3.0.1](https://gitlab.com/to-be-continuous/python/compare/3.0.0...3.0.1) (2021-12-10)


### Bug Fixes

* preserve explicit project dependencies versions when installing tools ([c0c9464](https://gitlab.com/to-be-continuous/python/commit/c0c9464782c71f1fa67d3ddb14ae437b17228a06))

# [3.0.0](https://gitlab.com/to-be-continuous/python/compare/2.2.0...3.0.0) (2021-11-20)


### Features

* fully integration of poetry ([f0406de](https://gitlab.com/to-be-continuous/python/commit/f0406debc207728ee5ea6c71e42357ad965f7c6a))


### refacto

* **py-doc:** remove Python doc build ([10a8150](https://gitlab.com/to-be-continuous/python/commit/10a8150e1d9d43f42458846e13aec19db68cccd8))


### BREAKING CHANGES

* **py-doc:** doc job removed
this job has to been rewritten :
	- it is in a wrong stage
	- needs an other tool (make)
	- generated doc is not publish anywhere
	- no ability to choise doc generation tool

in to-be-continuous, there is mkdocs template which is able to generate python doc too

# Conflicts:
#	templates/gitlab-ci-python.yml

# [2.2.0](https://gitlab.com/to-be-continuous/python/compare/2.1.1...2.2.0) (2021-11-15)


### Features

* move packaging to a separate stage ([945fc8a](https://gitlab.com/to-be-continuous/python/commit/945fc8a8c9d298640de2860b15c7162b1ad33684))

## [2.1.1](https://gitlab.com/to-be-continuous/python/compare/2.1.0...2.1.1) (2021-11-09)


### Bug Fixes

* Use PIP_OPTS for setup.py based install ([3ea29e6](https://gitlab.com/to-be-continuous/python/commit/3ea29e634c1e1a20a1f6f84f56ed8881e570d7c2))

## [2.0.3](https://gitlab.com/to-be-continuous/python/compare/2.0.2...2.0.3) (2021-10-12)


### Bug Fixes

* disable poetry usage (py-doc) ([73d5f2a](https://gitlab.com/to-be-continuous/python/commit/73d5f2a024f72c0b28b4ef10895bd8113ff7f932))

## [2.0.2](https://gitlab.com/to-be-continuous/python/compare/2.0.1...2.0.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([77af297](https://gitlab.com/to-be-continuous/python/commit/77af297de4d99257ee286d07b1d2837948887ac5))

## [2.0.1](https://gitlab.com/to-be-continuous/python/compare/2.0.0...2.0.1) (2021-10-04)


### Bug Fixes

* disable poetry usage ([17d57cb](https://gitlab.com/to-be-continuous/python/commit/17d57cb9626de30be281c147486745df78f78545))

## [2.0.0](https://gitlab.com/to-be-continuous/python/compare/1.3.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([4bb11b9](https://gitlab.com/to-be-continuous/python/commit/4bb11b9e2f971ba75fe1c4a36b9c1d475a6f1cb6))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.0](https://gitlab.com/to-be-continuous/python/compare/1.2.3...1.3.0) (2021-09-03)

### Features

* add Poetry extras support (PYTHON_POETRY_EXTRAS variable) ([e079e30](https://gitlab.com/to-be-continuous/python/commit/e079e305ddaf508d3105394df3fab1be92d6e38c))

## [1.2.3](https://gitlab.com/to-be-continuous/python/compare/1.2.2...1.2.3) (2021-07-26)

### Bug Fixes

* **poetry:** add option to disable poetry ([dbfe6f6](https://gitlab.com/to-be-continuous/python/commit/dbfe6f6b9abee4bf4aa68b603d015283a5e0bcc1))

## [1.2.2](https://gitlab.com/to-be-continuous/python/compare/1.2.1...1.2.2) (2021-06-24)

### Bug Fixes

* permission on reports directory ([f44e03a](https://gitlab.com/to-be-continuous/python/commit/f44e03a3afbad8c68babf51bc883da52bdf1c5b7))

## [1.2.1](https://gitlab.com/to-be-continuous/python/compare/1.2.0...1.2.1) (2021-06-23)

### Bug Fixes

* \"Missing git package for py-release job\" ([082f308](https://gitlab.com/to-be-continuous/python/commit/082f308330eb16a42704370190c66ba5a7823671))

## [1.2.0](https://gitlab.com/to-be-continuous/python/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([688d6f2](https://gitlab.com/to-be-continuous/python/commit/688d6f26374c4bc0610a0f979ed836c5e46c7754))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/python/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([73dbac6](https://gitlab.com/Orange-OpenSource/tbc/python/commit/73dbac6b81dcbe22b3fcfdbd34493b43fe8464a2))

## 1.0.0 (2021-05-06)

### Features

* initial release ([a1a8677](https://gitlab.com/Orange-OpenSource/tbc/python/commit/a1a867713c55fdc0ac17c3e7bd5540a7aee314cd))
